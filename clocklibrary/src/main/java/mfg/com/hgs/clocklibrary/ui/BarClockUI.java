package mfg.com.hgs.clocklibrary.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Bar 형태의 Clock UI 입니다.
 *
 * @author 함지성
 */
public class BarClockUI extends View implements UI, Ticker {

    private Thread tickThread;

    private BarClockUI ui;

    private float W, H;
    private Paint linePaint, fillPaint, textPaint;
    private int h, m, s;

    public BarClockUI(Context context) {
        super(context);
        initialize();
    }

    public BarClockUI(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public BarClockUI(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @Override
    public void initialize() {
        ui = this;

        // Set Paint

        // paint for a line
        linePaint = new Paint();
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setStrokeWidth(3f);
        linePaint.setColor(0xFF000000);
        linePaint.setAntiAlias(true);

        // paint for a text
        textPaint = new Paint();
        textPaint.setStyle(Paint.Style.STROKE);
        textPaint.setStrokeWidth(.5f);
        textPaint.setColor(0xFFFFFFFF);
        textPaint.setAntiAlias(true);

        // paint for a fill
        fillPaint = new Paint();
        fillPaint.setStyle(Paint.Style.FILL);
        fillPaint.setColor(0xFFFF0FFF);
        fillPaint.setAntiAlias(true);

        tickThread = TickThread.getInstance(this);
        tickThread.start();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // Initialize size of canvas
        W = getWidth();
        H = getHeight();

        float ox = W / 2, oy = H / 2;

        // Time Squares

        // 시간
        canvas.drawRect(0, 0, W / 3, H / 24 * h, fillPaint);
        // 분
        canvas.drawRect(W / 3, 0, W / 3 * 2, H / 60 * m, fillPaint);
        // 초
        canvas.drawRect(W / 3 * 2, 0, W, H / 60 * s, fillPaint);

        canvas.drawText(h + "시", W / 6, H / 24 * h / 2, textPaint);
        canvas.drawText(m + "분", W / 2, H / 60 * m / 2, textPaint);
        canvas.drawText(s + "초", W / 6 * 5, H / 60 * s / 2, textPaint);
        /*
        int i;
        // Ruler
        for (i = 0; i < 60; i++) {
            canvas.drawLine(0, perSec * i, 10f, perSec * i, linePaint);
        }
        */
    }

    @Override
    public void tick(int hour, int min, int sec) {
        h = hour;
        m = min;
        s = sec;
        if (ui != null)
            postInvalidate();
    }
}
