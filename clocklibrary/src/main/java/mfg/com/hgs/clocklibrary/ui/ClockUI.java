package mfg.com.hgs.clocklibrary.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * 시계 형태의 Clock UI 입니다.
 *
 * @author 함지성
 */
public class ClockUI extends View implements UI, Ticker {

    private ClockUI ui;

    private Thread tickThread;

    private Paint linePaint, fillPaint, secPaint, minPaint, hourPaint;
    private float W, H;
    private int h, m, s;

    public ClockUI(Context context) {
        super(context);
        initialize();
    }

    public ClockUI(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public ClockUI(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @Override
    public void initialize() {
        // get its instance
        ui = this;

        // draw  lines
        linePaint = new Paint();
        linePaint.setColor(Color.BLUE);
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setStrokeWidth(3.6f);
        linePaint.setAntiAlias(true);

        fillPaint = new Paint();
        fillPaint.setColor(0xEEAAAAAA);
        fillPaint.setStyle(Paint.Style.FILL);
        fillPaint.setAntiAlias(true);

        hourPaint = new Paint();
        hourPaint.setColor(Color.DKGRAY);
        hourPaint.setStyle(Paint.Style.STROKE);
        hourPaint.setStrokeWidth(5.5f);
        hourPaint.setAntiAlias(true);

        minPaint = new Paint();
        minPaint.setColor(Color.GRAY);
        minPaint.setStyle(Paint.Style.STROKE);
        minPaint.setStrokeWidth(3.3f);
        minPaint.setAntiAlias(true);

        secPaint = new Paint();
        secPaint.setColor(Color.LTGRAY);
        secPaint.setStyle(Paint.Style.STROKE);
        secPaint.setStrokeWidth(2.1f);
        secPaint.setAntiAlias(true);

        tickThread = TickThread.getInstance(this);
        tickThread.start();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // get the size of View
        W = getWidth();
        H = getHeight();

        float ox, oy, radius;

        int i;

        ox = W / 2f;
        oy = H / 2f;
        radius = W / 2f * 0.8f;

        // Clock UI

        // Draw a circle
        canvas.drawCircle(ox, oy, radius, linePaint);

        // Draw numbers
        for (i = 1; i <= 12; i++) {
            float x = ox + (float) (radius * 0.9 * Math.cos(2 * Math.PI * i / 12f - Math.PI / 2));
            float y = oy + (float) (radius * 0.9 * Math.sin(2 * Math.PI * i / 12f - Math.PI / 2));
            canvas.drawText(String.valueOf(i), x, y, secPaint);
        }

        float hR = 21f / 100;
        float mR = hR * 24 - (int) (hR * 24);
        float sR = mR * 60 - (int) (mR * 60);
        hR = hR >= 0.5f ? hR - 0.5f : hR;

        canvas.drawLine(ox, oy, ox + (float) (radius * 0.5 * Math.cos(2 * Math.PI * hR - Math.PI / 2)), oy + (float) (radius * 0.5 * Math.sin(2 * Math.PI * hR - Math.PI / 2)), hourPaint);
        canvas.drawLine(ox, oy, ox + (float) (radius * 0.65 * Math.cos(2 * Math.PI * mR - Math.PI / 2)), oy + (float) (radius * 0.65 * Math.sin(2 * Math.PI * mR - Math.PI / 2)), minPaint);
        canvas.drawLine(ox, oy, ox + (float) (radius * 0.85 * Math.cos(2 * Math.PI * sR - Math.PI / 2)), oy + (float) (radius * 0.85 * Math.sin(2 * Math.PI * sR - Math.PI / 2)), secPaint);
    }

    @Override
    public void tick(int hour, int min, int sec) {
        h = hour;
        m = min;
        s = sec;
        if (ui != null)
            postInvalidate();
    }
}
