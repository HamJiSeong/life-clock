package mfg.com.hgs.clocklibrary.ui;

/**
 * Ticker 인터페이스는 TickerThread에서 보내는 시간 정보를 수신하는 리스너로 사용합니다.
 *
 * @author 함지성
 */
interface Ticker {
    /**
     * 매 초마다 tick 함수가 호출 됩니다.
     *
     * @param hour 몇 시 (0~23)
     * @param min  몇 분 (0~59)
     * @param sec  몇 초 (0~59)
     */
    void tick(int hour, int min, int sec);
}
