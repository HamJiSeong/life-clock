package mfg.com.hgs.clocklibrary.ui;

/**
 * This interface defines the core functions of UI class.
 */
interface UI {
    /**
     * initialize() 함수에서는 View를 그리기 위한 초기화 작업을 합니다.
     */
    void initialize();
}
