package mfg.com.hgs.clocklibrary.ui;

import java.util.Calendar;

/**
 * TickThread 에서는 Clock UI 에서 매초마다 작동하는 tick 함수를 작동시킵니다.
 *
 * @author 함지성
 */
class TickThread extends Thread {

    private Ticker ticker;

    private TickThread(Ticker ticker) {
        this.ticker = ticker;
    }

    public static TickThread getInstance(Ticker ticker) {
        return new TickThread(ticker);
    }

    @Override
    public void run() {
        super.run();
        while (true) {
            int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            int min = Calendar.getInstance().get(Calendar.MINUTE);
            int sec = Calendar.getInstance().get(Calendar.SECOND);

            // Tick every moment time
            ticker.tick(hour, min, sec);

            try {
                sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
